#include "conv.h"
#include "std.h"

 /**************************************************************\
 |                   Symmetric convolutions                     |
 \**************************************************************/


/* compute horizontal smoothing with 3-sized mask
*/
template<typename type_in_t, typename type_out_t>
void _smooth_3_horiz(int tx, int ty, const int w_center, const int w_side, type_in_t* pixels, type_out_t* _res) {
  int y;
  int sum_w = 2*w_side + w_center;
  if(!sum_w)  sum_w=1;
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(y=0; y<ty; y++) {
    int x,pos = y*tx;
    type_out_t* res = _res + pos;
    *res++ = ( (w_center+w_side)*pixels[0+pos] + w_side*pixels[1+pos])/sum_w;
    for(x=1; x<tx-1; x++)
      *res++ = (w_side*pixels[x+1+pos] + w_center*pixels[x+pos] + w_side*pixels[x-1+pos])/sum_w;
    *res++ = ( (w_center+w_side)*pixels[x+pos] + w_side*pixels[x-1+pos])/sum_w;
  }
}


template<typename type_in_t, typename type_out_t>
void _smooth_5_horiz(int tx, int ty, 
                     const int w_center, const int w_side1, const int w_side2, 
                     type_in_t* pixels, type_out_t* _res) {
  int y;
  int sum_w = 2*(w_side1 + w_side2) + w_center;
  if(!sum_w)  sum_w=1;
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(y=0; y<ty; y++) {
    int x,pos = y*tx;
    type_out_t* res = _res + pos;
      x=0;
      *res++ = ( 
                w_side2 * pixels[x  +pos] +
                w_side1 * pixels[x  +pos] +
                w_center* pixels[x  +pos] +
                w_side1 * pixels[x+1+pos] +
                w_side2 * pixels[x+2+pos] ) / sum_w;
      x++;
      *res++ = ( 
                w_side2 * pixels[x-1+pos] +
                w_side1 * pixels[x-1+pos] +
                w_center* pixels[x  +pos] +
                w_side1 * pixels[x+1+pos] +
                w_side2 * pixels[x+2+pos] ) / sum_w;
    
    for(x=2; x<tx-2; x++)
      *res++ = ( 
                w_side2 * pixels[x-2+pos] +
                w_side1 * pixels[x-1+pos] +
                w_center* pixels[x  +pos] +
                w_side1 * pixels[x+1+pos] +
                w_side2 * pixels[x+2+pos] ) / sum_w;
    
      *res++ = ( 
                w_side2 * pixels[x-2+pos] +
                w_side1 * pixels[x-1+pos] +
                w_center* pixels[x  +pos] +
                w_side1 * pixels[x+1+pos] +
                w_side2 * pixels[x+1+pos] ) / sum_w;
      x++;
      *res++ = ( 
                w_side2 * pixels[x-2+pos] +
                w_side1 * pixels[x-1+pos] +
                w_center* pixels[x  +pos] +
                w_side1 * pixels[x  +pos] +
                w_side2 * pixels[x  +pos] ) / sum_w;
  }
}

template<typename type_in_t, typename type_out_t>
void _smooth_7_horiz(int tx, int ty, const int w_center, const int w_side1, const int w_side2, const int w_side3, 
                    type_in_t* pixels, type_out_t* _res) {
  int y;
  int sum_w = 2*(w_side1 + w_side2 + w_side3) + w_center;
  if(!sum_w)  sum_w=1;
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(y=0; y<ty; y++) {
    int x,pos = y*tx;
    type_out_t* res = _res + pos;
      x=0;
      *res++ = ( 
                w_side3 * pixels[x  +pos] +
                w_side2 * pixels[x  +pos] +
                w_side1 * pixels[x  +pos] +
                w_center* pixels[x  +pos] +
                w_side1 * pixels[x+1+pos] +
                w_side2 * pixels[x+2+pos] +
                w_side3 * pixels[x+3+pos] ) / sum_w;
      x++;
      *res++ = ( 
                w_side3 * pixels[x-1+pos] +
                w_side2 * pixels[x-1+pos] +
                w_side1 * pixels[x-1+pos] +
                w_center* pixels[x  +pos] +
                w_side1 * pixels[x+1+pos] +
                w_side2 * pixels[x+2+pos] +
                w_side3 * pixels[x+3+pos] ) / sum_w;
      x++;
      *res++ = ( 
                w_side3 * pixels[x-2+pos] +
                w_side2 * pixels[x-2+pos] +
                w_side1 * pixels[x-1+pos] +
                w_center* pixels[x  +pos] +
                w_side1 * pixels[x+1+pos] +
                w_side2 * pixels[x+2+pos] +
                w_side3 * pixels[x+3+pos] ) / sum_w;
    
    for(x=3; x<tx-3; x++)
      *res++ = ( 
                w_side3 * pixels[x-3+pos] +
                w_side2 * pixels[x-2+pos] +
                w_side1 * pixels[x-1+pos] +
                w_center* pixels[x  +pos] +
                w_side1 * pixels[x+1+pos] +
                w_side2 * pixels[x+2+pos] +
                w_side3 * pixels[x+3+pos] ) / sum_w;
    
      *res++ = ( 
                w_side3 * pixels[x-3+pos] +
                w_side2 * pixels[x-2+pos] +
                w_side1 * pixels[x-1+pos] +
                w_center* pixels[x  +pos] +
                w_side1 * pixels[x+1+pos] +
                w_side2 * pixels[x+2+pos] +
                w_side3 * pixels[x+2+pos] ) / sum_w;
      x++;
      *res++ = ( 
                w_side3 * pixels[x-3+pos] +
                w_side2 * pixels[x-2+pos] +
                w_side1 * pixels[x-1+pos] +
                w_center* pixels[x  +pos] +
                w_side1 * pixels[x+1+pos] +
                w_side2 * pixels[x+1+pos] +
                w_side3 * pixels[x+1+pos] ) / sum_w;
      x++;
      *res++ = ( 
                w_side3 * pixels[x-3+pos] +
                w_side2 * pixels[x-2+pos] +
                w_side1 * pixels[x-1+pos] +
                w_center* pixels[x  +pos] +
                w_side1 * pixels[x  +pos] +
                w_side2 * pixels[x  +pos] +
                w_side3 * pixels[x  +pos] ) / sum_w;
  }
}


/* compute vertical smoothing with 3-sized mask
*/
template<typename type_in_t, typename type_out_t>
void _smooth_3_vert(int tx, int ty, const int w_center, const int w_side, type_in_t* pixels, type_out_t* res) {
  int x,y,pos=0;
  int sum_w = 2*w_side + w_center;
  if(!sum_w)  sum_w=1;
  for(x=0; x<tx; x++,pos++)
    res[pos] = (  (w_center+w_side)*pixels[pos] + w_side*pixels[pos+tx])/sum_w;
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(y=1; y<ty-1; y++) {
    int x,pos = y*tx;
    for(x=0; x<tx; x++,pos++)
      res[pos] = ( w_side*pixels[pos+tx] + w_center*pixels[pos] + w_side*pixels[pos-tx])/sum_w;
  }
  pos = (ty-1)*tx;
  for(x=0; x<tx; x++,pos++)
    res[pos] = ( (w_center+w_side)*pixels[pos] + w_side*pixels[pos-tx])/sum_w;
}

template<typename type_in_t, typename type_out_t>
void _smooth_5_vert(int tx, int ty, const int w_center, const int w_side1, const int w_side2, type_in_t* pixels, type_out_t* res) {
  int x,y,pos=0;
  int sum_w = 2*(w_side1 + w_side2) + w_center;
  if(!sum_w)  sum_w=1;
  const int tx1=tx,tx2=2*tx;
  for(x=0; x<tx; x++,pos++)
      res[pos] = ( 
                  w_side2 * pixels[pos] + 
                  w_side1 * pixels[pos] + 
                  w_center* pixels[pos] + 
                  w_side1 * pixels[pos+tx1] + 
                  w_side2 * pixels[pos+tx2] 
                  )/sum_w;
  for(x=0; x<tx; x++,pos++)
      res[pos] = ( 
                  w_side2 * pixels[pos-tx1] + 
                  w_side1 * pixels[pos-tx1] + 
                  w_center* pixels[pos] + 
                  w_side1 * pixels[pos+tx1] + 
                  w_side2 * pixels[pos+tx2] 
                  )/sum_w;
  
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(y=2; y<ty-2; y++) {
    int x,pos = y*tx;
    for(x=0; x<tx; x++,pos++)
      res[pos] = ( 
                  w_side2 * pixels[pos-tx2] + 
                  w_side1 * pixels[pos-tx1] + 
                  w_center* pixels[pos] + 
                  w_side1 * pixels[pos+tx1] + 
                  w_side2 * pixels[pos+tx2] 
                  )/sum_w;
  }
  pos = (ty-2)*tx;
  for(x=0; x<tx; x++,pos++)
      res[pos] = ( 
                  w_side2 * pixels[pos-tx2] + 
                  w_side1 * pixels[pos-tx1] + 
                  w_center* pixels[pos] + 
                  w_side1 * pixels[pos+tx1] + 
                  w_side2 * pixels[pos+tx1] 
                  )/sum_w;
  for(x=0; x<tx; x++,pos++)
      res[pos] = ( 
                  w_side2 * pixels[pos-tx2] + 
                  w_side1 * pixels[pos-tx1] + 
                  w_center* pixels[pos] + 
                  w_side1 * pixels[pos] + 
                  w_side2 * pixels[pos] 
                  )/sum_w;
}
template<typename type_in_t, typename type_out_t>
void _smooth_7_vert(int tx, int ty, const int w_center, const int w_side1, const int w_side2, const int w_side3, 
                    type_in_t* pixels, type_out_t* res) {
  int x,y,pos=0;
  int sum_w = 2*(w_side1 + w_side2 + w_side3) + w_center;
  if(!sum_w)  sum_w=1;
  const int tx1=tx,tx2=2*tx,tx3=3*tx;
  for(x=0; x<tx; x++,pos++)
      res[pos] = ( 
                  w_side3 * pixels[pos] + 
                  w_side2 * pixels[pos] + 
                  w_side1 * pixels[pos] + 
                  w_center* pixels[pos] + 
                  w_side1 * pixels[pos+tx1] + 
                  w_side2 * pixels[pos+tx2] + 
                  w_side3 * pixels[pos+tx3] 
                  )/sum_w;
  for(x=0; x<tx; x++,pos++)
      res[pos] = ( 
                  w_side3 * pixels[pos-tx1] + 
                  w_side2 * pixels[pos-tx1] + 
                  w_side1 * pixels[pos-tx1] + 
                  w_center* pixels[pos] + 
                  w_side1 * pixels[pos+tx1] + 
                  w_side2 * pixels[pos+tx2] + 
                  w_side3 * pixels[pos+tx3] 
                  )/sum_w;
  for(x=0; x<tx; x++,pos++)
      res[pos] = ( 
                  w_side3 * pixels[pos-tx2] + 
                  w_side2 * pixels[pos-tx2] + 
                  w_side1 * pixels[pos-tx1] + 
                  w_center* pixels[pos] + 
                  w_side1 * pixels[pos+tx1] + 
                  w_side2 * pixels[pos+tx2] + 
                  w_side3 * pixels[pos+tx3] 
                  )/sum_w;
  
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(y=3; y<ty-3; y++) {
    int x,pos = y*tx;
    for(x=0; x<tx; x++,pos++)
      res[pos] = ( 
                  w_side3 * pixels[pos-tx3] + 
                  w_side2 * pixels[pos-tx2] + 
                  w_side1 * pixels[pos-tx1] + 
                  w_center* pixels[pos] + 
                  w_side1 * pixels[pos+tx1] + 
                  w_side2 * pixels[pos+tx2] + 
                  w_side3 * pixels[pos+tx3] 
                  )/sum_w;
  }
  pos = (ty-3)*tx;
  for(x=0; x<tx; x++,pos++)
      res[pos] = ( 
                  w_side3 * pixels[pos-tx3] + 
                  w_side2 * pixels[pos-tx2] + 
                  w_side1 * pixels[pos-tx1] + 
                  w_center* pixels[pos] + 
                  w_side1 * pixels[pos+tx1] + 
                  w_side2 * pixels[pos+tx2] + 
                  w_side3 * pixels[pos+tx2] 
                  )/sum_w;
  for(x=0; x<tx; x++,pos++)
      res[pos] = ( 
                  w_side3 * pixels[pos-tx3] + 
                  w_side2 * pixels[pos-tx2] + 
                  w_side1 * pixels[pos-tx1] + 
                  w_center* pixels[pos] + 
                  w_side1 * pixels[pos+tx1] + 
                  w_side2 * pixels[pos+tx1] + 
                  w_side3 * pixels[pos+tx1] 
                  )/sum_w;
  for(x=0; x<tx; x++,pos++)
      res[pos] = ( 
                  w_side3 * pixels[pos-tx3] + 
                  w_side2 * pixels[pos-tx2] + 
                  w_side1 * pixels[pos-tx1] + 
                  w_center* pixels[pos] + 
                  w_side1 * pixels[pos] + 
                  w_side2 * pixels[pos] + 
                  w_side3 * pixels[pos] 
                  )/sum_w;
}



 /**************************************************************\
 |                    Smoothing / Blurring                      |
 \**************************************************************/


template<typename type_in_t, typename type_out_t>
void _smooth_121_vert(int tx, int ty, type_in_t* pixels, type_out_t* res) {
  _smooth_3_vert( tx, ty, 2, 1, pixels, res );
}
template<typename type_in_t, typename type_out_t>
void _smooth_121_horiz(int tx, int ty, type_in_t* pixels, type_out_t* res) {
  _smooth_3_horiz( tx, ty, 2, 1, pixels, res );
}
// explicit instanciation
template void _smooth_121_vert(int tx, int ty, UBYTE* pixels, UBYTE* res);
template void _smooth_121_vert(int tx, int ty, float* pixels, float* res);
template void _smooth_121_horiz(int tx, int ty, UBYTE* pixels, UBYTE* res);
template void _smooth_121_horiz(int tx, int ty, float* pixels, float* res);

template<typename type_in_t, typename type_out_t>
void _smooth_121(int tx, int ty, type_in_t* pixels, type_out_t* res) {
  type_out_t* tmp = NEWA(type_out_t,tx*ty);
  _smooth_121_horiz( tx, ty, pixels, tmp );
  _smooth_121_vert( tx, ty, tmp, res );
  free(tmp);
}

/* Smooth an image with [1,2,1] filter 
   in-place is ok (img can be ==res)
*/
void _smooth_121_B(UBYTE_image* pixels, UBYTE_image* res) {
  ASSERT_SAME_SIZE(pixels,res);
  _smooth_121(pixels->tx,pixels->ty,pixels->pixels,res->pixels);
}
void _smooth_121_f(float_image* pixels, float_image* res) {
  ASSERT_SAME_SIZE(pixels,res);
  _smooth_121(pixels->tx,pixels->ty,pixels->pixels,res->pixels);
}

/* General convolution with symmetric filters
   in-place is ok (img can be ==res)
*/
template<typename type_in_t, typename type_out_t>
void _sym_filter3(int tx, int ty, type_in_t* pixels, int_Tuple2* filter, type_out_t* res) {
  type_out_t* tmp = NEWA(type_out_t,tx*ty);
  _smooth_3_horiz( tx, ty, filter[0], filter[1], pixels, tmp );
  _smooth_3_vert(  tx, ty, filter[0], filter[1], tmp, res );
  free(tmp);
}
template void _sym_filter3(int tx, int ty, UBYTE* pixels, int_Tuple2* filter, UBYTE* res);
template void _sym_filter3(int tx, int ty, float* pixels, int_Tuple2* filter, float* res);

template<typename type_in_t, typename type_out_t>
void _sym_filter5(int tx, int ty, type_in_t* pixels, int_Tuple3* filter, type_out_t* res) {
  type_out_t* tmp = NEWA(type_out_t,tx*ty);
  _smooth_5_horiz( tx, ty, filter[0], filter[1], filter[2], pixels, tmp );
  _smooth_5_vert(  tx, ty, filter[0], filter[1], filter[2], tmp, res );
  free(tmp);
}
template void _sym_filter5(int tx, int ty, UBYTE* pixels, int_Tuple3* filter, UBYTE* res);
template void _sym_filter5(int tx, int ty, float* pixels, int_Tuple3* filter, float* res);

template<typename type_in_t, typename type_out_t>
void _sym_filter7(int tx, int ty, type_in_t* pixels, int_Tuple4* filter, type_out_t* res) {
  type_out_t* tmp = NEWA(type_out_t,tx*ty);
  _smooth_7_horiz( tx, ty, filter[0], filter[1], filter[2], filter[3], pixels, tmp );
  _smooth_7_vert(  tx, ty, filter[0], filter[1], filter[2], filter[3], tmp, res );
  free(tmp);
}
template void _sym_filter7(int tx, int ty, UBYTE* pixels, int_Tuple4* filter, UBYTE* res);
template void _sym_filter7(int tx, int ty, float* pixels, int_Tuple4* filter, float* res);

void _sym_filter3_f(float_image* pixels, int_Tuple2* filter, float_image* res) {
  ASSERT_SAME_SIZE(pixels,res);
  _sym_filter3(pixels->tx,pixels->ty,pixels->pixels,filter,res->pixels);
}
void _sym_filter5_f(float_image* pixels, int_Tuple3* filter, float_image* res) {
  ASSERT_SAME_SIZE(pixels,res);
  _sym_filter5(pixels->tx,pixels->ty,pixels->pixels,filter,res->pixels);
}
void _sym_filter7_f(float_image* pixels, int_Tuple4* filter, float_image* res) {
  ASSERT_SAME_SIZE(pixels,res);
  _sym_filter7(pixels->tx,pixels->ty,pixels->pixels,filter,res->pixels);
}


/* Smooth an image using a Gaussian filter.
   in-place is ok (img can be ==res)
*/
template<typename type_t>
void _smooth_gaussian_alltype( const int tx, const int ty, type_t* img, float _sigma, type_t* res ) {
  const float MAX_SIGMA = 1.86f;
  
  type_t* img2 = img;
  if(_sigma>MAX_SIGMA) {  // reallocate if more than one smoothing pass is required
    img2 = NEWA(type_t,tx*ty);
    memcpy(img2,img,tx*ty*sizeof(type_t));
  }
  type_t* tmp = NEWA(type_t,tx*ty);
  type_t* old_res = res;
  
  float remaining = _sigma*_sigma;
  while( 1 ) {
    float sigma = MIN(MAX_SIGMA,sqrt(remaining));
    remaining -= sigma*sigma;
    
    // compute gaussian filter coefficients
    const int wcenter = 1000;
    const int wside1 = int(0.5 + wcenter*exp( -pow2(1./sigma)/2 ));
    const int wside2 = int(0.5 + wcenter*exp( -pow2(2./sigma)/2 ));
    const int wside3 = int(0.5 + wcenter*exp( -pow2(3./sigma)/2 ));
    const int wside4 = int(0.5 + wcenter*exp( -pow2(4./sigma)/2 ));
    assert( wside4 < wcenter/10 || !"error: smoothing is too large" );
    
    if ( wside2 < wcenter/10 ) {
      _smooth_3_horiz( tx, ty, wcenter, wside1, img2, tmp );
      _smooth_3_vert(  tx, ty, wcenter, wside1, tmp, res );
    } else if( wside3 < wcenter/10 ) {
      _smooth_5_horiz( tx, ty, wcenter, wside1, wside2, img2, tmp );
      _smooth_5_vert(  tx, ty, wcenter, wside1, wside2, tmp, res );
    } else {
      _smooth_7_horiz( tx, ty, wcenter, wside1, wside2, wside3, img2, tmp );
      _smooth_7_vert(  tx, ty, wcenter, wside1, wside2, wside3, tmp, res );
    }
    
    if(remaining < 0.001)
      break;
    else {
      type_t* tmp3;
      tmp3 = img2;
      img2 = res;
      res = tmp3;
    }
  }
  
  if(res!=old_res) { // copy to true res
    memcpy(old_res,res,tx*ty*sizeof(type_t));
    img2 = res;
  }
  if(_sigma>MAX_SIGMA) 
    free(img2);
  free(tmp);
}

/* Gaussian filter an image
   in-place is ok (img can be ==res)
*/
void _smooth_gaussian_B( UBYTE_image* img, float _sigma, UBYTE_image* res ) {
  ASSERT_SAME_SIZE(img,res);
  _smooth_gaussian_alltype(img->tx,img->ty,img->pixels,_sigma,res->pixels);
}
void _smooth_gaussian_f( float_image* img, float _sigma, float_image* res ) {
  ASSERT_SAME_SIZE(img,res);
  _smooth_gaussian_alltype(img->tx,img->ty,img->pixels,_sigma,res->pixels);
}


/* Gaussian filter a multi-layers image
   in-place is ok (img can be ==res)
*/
void _smooth_gaussian_layers_B( UBYTE_layers* img, float sigma, UBYTE_layers* res ) {
  int l;
  const int npix = img->tx*img->ty;
  for(l=0; l<img->tz; l++)
    _smooth_gaussian_alltype(img->tx,img->ty,img->pixels+l*npix,sigma,res->pixels+l*npix);
}
void _smooth_gaussian_layers_f( float_layers* img, float sigma, float_layers* res ) {
  int l;
  const int npix = img->tx*img->ty;
  for(l=0; l<img->tz; l++)
    _smooth_gaussian_alltype(img->tx,img->ty,img->pixels+l*npix,sigma,res->pixels+l*npix);
}
































