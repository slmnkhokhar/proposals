#ifndef ___numpy_image___
#define ___numpy_image___

typedef unsigned char UBYTE;
typedef unsigned int UINT;


/************************
* Tuples
*/

#define DEFINE_TUPLE(type,nval)  \
    typedef type type ## _tuple ## nval [nval];

DEFINE_TUPLE(int,2)
DEFINE_TUPLE(int,3)
DEFINE_TUPLE(int,4)
DEFINE_TUPLE(float,2)
DEFINE_TUPLE(float,3)
DEFINE_TUPLE(float,4)


/************************
* Tuples2 more convienent
*/

#define DEFINE_TUPLE2(type,nval)  \
    typedef type type ## _Tuple ## nval;

DEFINE_TUPLE2(int,2)
DEFINE_TUPLE2(int,3)
DEFINE_TUPLE2(int,4)
DEFINE_TUPLE2(float,2)
DEFINE_TUPLE2(float,3)
DEFINE_TUPLE2(float,4)
DEFINE_TUPLE2(float,6)
DEFINE_TUPLE2(float,8)


/************************
* 1D Array
*/

#define DEFINE_ARRAY(type)  \
    typedef struct {  \
      type* pixels; \
      int tx; \
    } type##_array;

DEFINE_ARRAY(UBYTE)
DEFINE_ARRAY(int)
DEFINE_ARRAY(UINT)
DEFINE_ARRAY(float)

#define ASSERT_ARRAY_ZEROS(arr) {int size=arr->tx; assert((arr->pixels[0]==0 && arr->pixels[size/2]==0 && arr->pixels[size-1]==0) || !"error: matrix " #arr "is supposed to be zeros");}


/************************
* 2D Image
*/

#define DEFINE_IMG(type)    \
    typedef struct { \
      type* pixels;\
      int tx,ty;\
    } type##_image;

DEFINE_IMG(UBYTE)
DEFINE_IMG(int)
DEFINE_IMG(UINT)
DEFINE_IMG(float)

#define ASSERT_SAME_SIZE  ASSERT_SAME_IMG_SIZE
#define ASSERT_IMG_SIZE  ASSERT_SAME_IMG_SIZE
#define ASSERT_SAME_IMG_SIZE(im1,im2)  if(im1 && im2)  assert(im1->tx==im2->tx && im1->ty==im2->ty);

#define ASSERT_IMAGE_ZEROS
#define ASSERT_IMG_ZEROS(img) {int size=img->tx*img->ty; assert((img->pixels[0]==0 && img->pixels[size/2]==0 && img->pixels[size-1]==0) || !"error: matrix " #img "is supposed to be zeros");}
#define IMG_SIZE(cube) ((cube)->tx*(cube)->ty)


/************************
* RGB Image
*/

#define DEFINE_IMG3(type) \
    typedef struct {  \
      type* pixels;  \
      int tx,ty;  \
    } type##_image3;

DEFINE_IMG3(UBYTE)
DEFINE_IMG3(float)


/************************
* 3D Image = Cube (Z coordinates are contiguous)
*/

#define DEFINE_CUBE(type) \
    typedef struct {  \
      type* pixels;  \
      int tx,ty,tz;  \
    } type##_cube;

DEFINE_CUBE(UBYTE)
DEFINE_CUBE(short)
DEFINE_CUBE(int)
DEFINE_CUBE(UINT)
DEFINE_CUBE(float)

#define ASSERT_SAME_CUBE_SIZE(im1, im2)   \
  if((im1) && (im2))  assert((im1)->tx==(im2)->tx && (im1)->ty==(im2)->ty && (im1)->tz==(im2)->tz);

#define ASSERT_CUBE_ZEROS(img) {int size=img->tx*img->ty*img->tz; assert((img->pixels[0]==0 && img->pixels[size/2]==0 && img->pixels[size-1]==0) || !"error: matrix " #img "is supposed to be zeros");}
#define CUBE_SIZE(cube) ((cube)->tx*(cube)->ty*(cube)->tz)


/************************
* 3D Image = concatenation of XY layers
*/

#define DEFINE_LAYERS(type) \
    typedef struct {  \
      type* pixels;  \
      int tx,ty,tz;  \
    } type##_layers;

DEFINE_LAYERS(UBYTE)
DEFINE_LAYERS(int)
DEFINE_LAYERS(UINT)
DEFINE_LAYERS(float)


#define ASSERT_SAME_LAYERS_SIZE(im1,im2)  ASSERT_SAME_CUBE_SIZE(im1,im2)
#define ASSERT_LAYERS_ZEROS ASSERT_CUBE_ZEROS
#define LAYERS_SIZE(layers)   CUBE_SIZE(layers)


/************************
* Sparse CSR matrix
*/

typedef struct  {
  int* indices; //  indices of columns
  int* indptr;  //  indices of indices of rows
  float* data;  //  row i contains values data[indptr[i]:indptr[i+1]] at columns indices[indptr[i]:indptr[i+1]]
  int nr,nc;
} csr_matrix;

#define ASSERT_SAME_CSR_SIZE(im1,im2)  assert(im1->nr==im2->nr && im1->nc==im2->nc)


/************************
* Sparse COO matrix
*/

typedef struct  {
  int* row;
  int* col;
  float* data;
  int nr,nc,n_elem;
} coo_matrix;

#define ASSERT_SAME_COO_SIZE(im1,im2)  ASSERT_SAME_CSR_SIZE(im1,im2)



/*****************
  creation, reshaping macros
*/

#define empty_array(type,tx)        ((type##_array){NEWA(type,(tx)),tx})
#define empty_image(type,tx,ty)     ((type##_image){NEWA(type,(tx)*(ty)),tx,ty})
#define empty_cube(type,tx,ty,tz)   ((type##_cube  ){NEWA(type,(tx)*(ty)*(tz)),tx,ty,tz})
#define empty_layers(type,tx,ty,tz) ((type##_layers){NEWA(type,(tx)*(ty)*(tz)),tx,ty,tz})

#define zeros_array(type,tx)        ((type##_array){NEWAC(type,(tx)),tx})
#define zeros_image(type,tx,ty)     ((type##_image){NEWAC(type,(tx)*(ty)),tx,ty})
#define zeros_cube(type,tx,ty,tz)   ((type##_cube  ){NEWAC(type,(tx)*(ty)*(tz)),tx,ty,tz})
#define zeros_layers(type,tx,ty,tz) ((type##_layers){NEWAC(type,(tx)*(ty)*(tz)),tx,ty,tz})

#define array_like(type,l)     ((type##_array){NEWA(type,(l)->tx),(l)->tx})
#define image_like(type,l)     ((type##_image){NEWA(type,(l)->tx*(l)->ty),(l)->tx,(l)->ty})
#define cube_like(type,l)      ((type##_cube  ){NEWA(type,(l)->tx*(l)->ty*(l)->tz),(l)->tx,(l)->ty,(l)->tz})
#define layers_like(type,l)    ((type##_layers){NEWA(type,(l)->tx*(l)->ty*(l)->tz),(l)->tx,(l)->ty,(l)->tz})


#define set_array(img, val)     memset((img)->pixels, val, (img)->tx*sizeof((img)->pixels[0]))
#define set_image(img, val)     memset((img)->pixels, val, (img)->tx*(img)->ty*sizeof((img)->pixels[0]))
#define set_cube(img, val)      memset((img)->pixels, val, (img)->tx*(img)->ty*(img)->tz*sizeof((img)->pixels[0]))
#define set_layers(img, val)    set_cube(img, val)

#define free_array( arr )       {free((arr)->pixels); (arr)->pixels=0;}


#define reshape_xy(type, arr)   ((type##_array){(arr)->pixels, (arr)->tx*(arr)->ty})
#define reshape_xyz(type, arr)  ((type##_array){(arr)->pixels, (arr)->tx*(arr)->ty*(arr)->tz})
#define reshape_xy_z(type, arr) ((type##_image){(arr)->pixels, (arr)->tx*(arr)->ty, (arr)->tz})
#define reshape_z_xy(type, arr) ((type##_image){(arr)->pixels, (arr)->tz, (arr)->tx*(arr)->ty})
#define reshape_x_yz(type, arr) ((type##_image){(arr)->pixels, (arr)->tx, (arr)->ty*(arr)->tz})


#endif


































