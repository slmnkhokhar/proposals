
import os


class Dataset(object):

    def __init__(self, dataset_name):
        self.base_path = '../data/' + dataset_name

    def get_images_path(self, video):
        return os.path.join(self.base_path, 'frames', video, 'png')

    def get_edges_path(self, video):
        return os.path.join(self.base_path, 'edges', video)

    def get_flow_path(self, video, direction):
        return os.path.join(self.base_path, 'flow', direction, video)

    def get_segmentation_directory(self, video):
        return os.path.join(self.base_path, 'segmentations', video)

    def get_tree_path(self, video):
        return os.path.join(self.base_path, 'hierarchies', '%s.npz')


DATASETS = {
    'ucf_sports': Dataset('ucf_sports'),
    'kth': Dataset('kth'),
}

