#include "numpy_image.h"
#include "weighted_dist_trf.h"



/* output an image of dense (x,y) coordinates (x in [0,tx[ and y in [0,ty[)
*/
void _xy_field( int_cube* res);



/* Compute the closest seeds for each pixel given a euclidean distance.
*/
void _euclidean_nnfield( float_image* seeds, int nn, int_cube* best, float_cube* dis, int n_thread );



/* Select the best subset based on a median filter
  results are written in place in best[:,:nr] and dis[:,:nr]
*/
void _select_median_nn( int_image* best, int nr, float_image* vecs, float_image* dis, int n_thread );




/* Compute the closest seeds for each pixel given a distance transform cost function.
*/
void _dist_trf_nnfield( int_image* seeds, int nn, float dmax, float_image* cost, dt_params_t* dt_params,
                        int_cube* best, float_cube* dis, int n_thread );




/* compute a graph of neighboring seeds based on euclidean distance
*/
void _ngh_seeds_euclidean( int_image* _seeds, int nn, float_image* cost, 
                           int_image* _nnf, float_image* _dis, csr_matrix* res_out, int n_thread );

/* Compute fast approximate distance between pairs of seeds, given a cost map.
   The function modifies <ngh> data term.
*/
void compute_pairwise_dist_trf( int_image* _seeds, csr_matrix* ngh, float_image* cost, int n_thread );


/* Find neighboring labels and store their relation in a sparse matrix.
   linkage = 's' (single == mininum) 'a' (average) or 'm' (maximum)
   nb_min: neighbors with less than nb_min border pixel are pruned.
*/
void _ngh_labels_to_spmat( int ns, int_image* labels, float_image* dmap, char linkage, int nb_min, csr_matrix* res_out );


/* Compute the closest <nn> seeds for each seed given a distance transform cost function.
*/
void _dist_trf_seeds_fast( int_image* seeds, int bdry_mode,
                           float_image* cost, char linkage, int min_border_size, dt_params_t* dt_params,
                           int_image* labels, float_image* dmap,
                           int_image* nnf, float_image* dis, int n_thread );

/* Compute the closest <nn> seeds for each pixel given a distance transform cost function.
   
   bdry_mode: how the distances between seeds are computed:
      =0 => estimated from boundaries in label map (more precise)
      >0 => estimated using line cost between matches center (faster).
            allocate <bdry_mode> euclidean neighbor per match, default = 6
*/
void _dist_trf_nnfield_fast( int_image* seeds, int bdry_mode,
                             float_image* cost, char linkage, int min_border_size, dt_params_t* dt_params,
                             int_cube* best, float_cube* dist, int n_thread );

/* Compute the closest <nn> seeds for a list of given pixels  depending on 
   a distance transform cost function.
*/
void _dist_trf_nnfield_subset( int_image* seeds, int bdry_mode,
                               float_image* cost, char linkage, int min_border_size, dt_params_t* dt_params,
                               int_image* pixels, int_image* best, float_image* dist, int n_thread );


/* Compute the closest seeds for each pixel given a distance transform cost function.
   The function returns at least <nmin> neighbors per pixel.
*/
void _dist_trf_dmaxfield_fast( int_image* seeds, int bdry_mode, float dmax, int nmin, int nmax, 
                               float_image* cost, char linkage, int min_border_size,
                               dt_params_t* dt_params, csr_matrix* res_out, int n_thread );



/* Select the best subset based on a median filter for sparse matrix
   ratio: ratio of nn that are filtered (eg. 0.5 means half)
*/
void select_median_dmax( csr_matrix* best, float ratio, float_image* vecs, int n_thread );



/* Kernel interpolation for sparse matrix
  Result for each dimension is x = \sum(w_i*x_i) / \sum(w_i)
*/
void _kernel_interpolate_csr( csr_matrix* nnf, float_image* vects, float_image* res, int n_thread );



/* Extract a lot of regions given a distance transform cost function.
  dmax: max distance to form a region
  dmin: distance thr to avoid picking a seed in a previously found region
*/
void _random_regions_dist_trf( float dmin, float dmax, float_image* cost, dt_params_t* dt_params,
                               csr_matrix* res_out, int n_thread );


/* visualize distances between neighboring nodes
*/
void _viz_edge_weights( const csr_matrix* edges, int_image* labels, float_image* _img );


/* Compute the closest seeds for each seed given a distance transform cost function.
   The function returns at least <nmin> neighbors per pixel.
*/
void _dist_trf_dmax_seeds_fast( int_image *labels, int_image* seeds, int bdry_mode, float dmax, int nmin, int nmax, 
                               float_image* cost, char linkage, int min_border_size,
                               dt_params_t* dt_params, csr_matrix* res_out, int n_thread );

void _fit_nadarayawatson(float_image *newvects, const csr_matrix *nnf, const int_image *seeds, const float_image *vects, int n_thread);
void _apply_nadarayawatson(const float_image *seedsvects, float_image *newvects, const int_image *labels, int n_thread);

