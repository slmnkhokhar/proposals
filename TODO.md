
[ ] Remove the wildcard import from `svx/viz_tree.py`.
[ ] PEP8 the script with flow utilities `svx/flow_utils.py`.
[ ] Check that in Jerome's Makefiles the flags are passed as the last arguments.

Observations:

* In the SWIG files, the C functions start with an underscore.

